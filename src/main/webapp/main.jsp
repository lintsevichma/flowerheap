<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>FlowerHeap</title>

    <meta charset="UTF-8">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <!-- Viewport meta tag -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
</head>

<body>
<header>
    <jsp:include page="parts/header.jsp" />
</header>

<div class="container mt-4">

    <h4> You can make a bouquet as you like: </h4>

    <form method="post">
        <div class="card-columns">
            <c:forEach var="flowerInBouquet" items="${flowers}">
                <div class="card bg-light">
                    <div class="card-header">
                            ${flowerInBouquet.flower.type}, ${flowerInBouquet.flower.color}
                    </div>
                    <div class="card-body">
                        <p class="card-text">${flowerInBouquet.flower.price} $/unit</p>
                        <input type="hidden" name="flowerId" value="${flowerInBouquet.flower.id}" />
                        <label for="flowerNum"> Value </label><input type="number" name="flowerNum" value="${flowerInBouquet.flowerNum}" min="0" max="105" id="flowerNum">
                    </div>
                </div>
            </c:forEach>
        </div>

        <div class="card-columns mb-6">
            <c:forEach var="design" items="${designs}">
                <div class="card text-white bg-dark">
                    <div class="card-header">
                            ${design.type}
                    </div>
                    <div class="card-body">
                        <p class="card-text">${design.price} $/unit</p>
                        <label>
                            <input type="radio" name="designId" value="${design.id}" checked/>
                        </label>
                    </div>
                </div>
            </c:forEach>
        </div>
        <input type="submit" value="To order" />
    </form>
</div>

<!-- Optional JavaScript -->
<!-- jQuery -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<!-- Popper.js -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<!-- Bootstrap JS -->
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>