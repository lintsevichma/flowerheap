<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>FlowerHeap</title>

    <meta charset="UTF-8">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <!-- Viewport meta tag -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
</head>

<body>
<header>
    <jsp:include page="parts/header.jsp" />
</header>

<div class="container">

    <c:if test="${not empty emptyMessage}">
    <div>${emptyMessage}</div>
    </c:if>

    <div class="card-columns">
        <c:forEach var="bouquet" items="${bouquetList}">
        <div class="card" style="width: 18rem;">
            <div class="card-body">
                <h5 class="card-title">${bouquet.value.design.type}
                    <form method="post">
                        <input type="hidden" name="bouquetId" value="${bouquet.key}">
                        <input type="submit" value="Click" name="remove" />
                    </form>
                </h5>
                <h6 class="card-subtitle mb-2 text-muted">
                    <label>
                        Quantity: <input type="number" name="bouquetQuantity" value="${bouquet.value.quantity}" max="5" min="1">
                    </label>
                </h6>
                <p class="card-text">
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">Type</th>
                        <th scope="col">Color</th>
                        <th scope="col">Quantity</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach var="flowerInBouquet" items="${bouquet.value.flowerList}">
                        <tr>
                            <td>${flowerInBouquet.flower.type}</td>
                            <td>${flowerInBouquet.flower.color}</td>
                            <td>${flowerInBouquet.flowerNum}</td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
                </p>
            </div>
        </div>
        </c:forEach>
        <div class="card-deck">

            <form method="post">
        <label for="name"> Name: </label> <input type="text" name="name" id="name" /> <br>
        <label for="surname"> Surname: </label> <input type="text" name="surname" id="surname" /> <br>
        <label for="address"> Address: </label> <input type="text" name="address" id="address" /> <br>
        <label for="phoneNumber"> Phone: </label> <input type="text" name="phoneNumber" id="phoneNumber" /> <br>

        <input type="submit" value="Order">
    </form>

</div>

<!-- Optional JavaScript -->
<!-- jQuery -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<!-- Popper.js -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<!-- Bootstrap JS -->
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>
