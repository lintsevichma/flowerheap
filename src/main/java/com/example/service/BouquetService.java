package com.example.service;

import com.example.dao.BouquetDao;
import com.example.dao.DesignDao;
import com.example.dao.FlowerDao;
import com.example.dao.OrderDao;
import com.example.model.FlowerInBouquet;
import com.example.model.entity.Bouquet;
import com.example.model.entity.Design;
import com.example.model.entity.Flower;
import com.example.model.entity.Order;

import java.util.*;

public class BouquetService {
    private OrderDao orderDao = new OrderDao();
    private BouquetDao bouquetDao = new BouquetDao();
    private FlowerDao flowerDao = new FlowerDao();
    private DesignDao designDao = new DesignDao();

    public List<FlowerInBouquet> getFlowerList() {
        List<FlowerInBouquet> flowerList = new ArrayList<>();
        List<Flower> AllFlowers = flowerDao.getAll();
        for (Flower flower : AllFlowers) {
            FlowerInBouquet flowerInBouquet = new FlowerInBouquet(flower, 0);
            flowerList.add(flowerInBouquet);
        }
        return flowerList;
    }

    public List<Design> getDesignList() {
        return designDao.getAll();
    }

    private List<FlowerInBouquet> selectFlowers(String[] selectedFlower, String[] flowerNums) {
        List<FlowerInBouquet> flowers = new ArrayList<>();
        Map<Long, Integer> flower = new HashMap<>();
        for (int i = 0; i < selectedFlower.length; i++) {
            Long flowerId = Long.parseLong(selectedFlower[i]);
            Integer flowerNum = Integer.parseInt(flowerNums[i]);
            flower.put(flowerId, flowerNum);
        }
        for (Map.Entry<Long, Integer> item : flower.entrySet()) {
            if (item.getValue() != 0) {
                FlowerInBouquet flowerInBouquet = new FlowerInBouquet(flowerDao.getByKey(item.getKey()), item.getValue());
                flowers.add(flowerInBouquet);
            }
        }
        return flowers;
    }

    public Bouquet initBouquet(Long designId, String[] selectedFlower, String[] flowerNums) {
        Bouquet bouquet = new Bouquet();
        bouquet.setUuid(UUID.randomUUID().toString());
        bouquet.setDesign(designDao.getByKey(designId));
        bouquet.setFlowerList(selectFlowers(selectedFlower, flowerNums));
        bouquet.setQuantity(1);

        return bouquet;
    }

    public void addNewBouquet(Bouquet bouquet, Order order) {
        bouquet.setOrder(order);
        bouquetDao.create(bouquet);
    }

    private Map<String, Bouquet> bouquetList = new HashMap<>();
    public Map<String, Bouquet> addBouquetInList(String uuid, Bouquet bouquet) {
        bouquetList.put(uuid, bouquet);
        return bouquetList;
    }
}