package com.example.service;

import com.example.dao.UserDao;
import com.example.model.entity.User;

import java.util.UUID;

public class UserService {
    private UserDao userDao = new UserDao();

    public void addNewUser(String username, String password, String email) {
        try {
            User user = new User();
            user.setEmail(email);
            user.setPassword(password);
            user.setUsername(username);
            user.setActivationCode(UUID.randomUUID().toString());
            user.setActive(true);
            user.setDiscount(0.05F);

            userDao.create(user);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean loginValid(String email,String password) {
        return userDao.loginValid(email, password);
    }

    public boolean emailValid(String email) {
        return userDao.userExists(email);
    }

    public User findUserByEmail(String email) {
        return userDao.getByKey(userDao.findUserByEmail(email));
    }
}
