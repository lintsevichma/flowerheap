package com.example.service;

import com.example.dao.OrderDao;
import com.example.model.entity.Bouquet;
import com.example.model.entity.Order;
import com.example.model.entity.User;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class OrderService {
    private OrderDao orderDao = new OrderDao();

    public Order addNewOrder(String name, String surname, String address, String phoneNumber, Map<String, Bouquet> bouquets) {
        Order order = new Order();
        order.setName(name);
        order.setSurname(surname);
        order.setAddress(address);
        order.setPhoneNumber(phoneNumber);
        order.setBouquets(getBouquetList(bouquets));

        return orderDao.create(order);
    }
    public Order addNewOrder(String name, String surname, String address, String phoneNumber, User user, Map<String, Bouquet> bouquets) {
        Order order = new Order();
        order.setName(name);
        order.setSurname(surname);
        order.setAddress(address);
        order.setPhoneNumber(phoneNumber);
        order.setBouquets(getBouquetList(bouquets));
        order.setUser(user);

        return orderDao.createWithUser(order);
    }

    public List<Bouquet> getBouquetList(Map<String, Bouquet> bouquets) {
        List<Bouquet> bouquetList = new ArrayList<>(bouquets.values());
        return bouquetList;
    }
}
