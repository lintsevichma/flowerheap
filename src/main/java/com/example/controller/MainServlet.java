package com.example.controller;

import com.example.model.entity.Bouquet;
import com.example.service.BouquetService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet(urlPatterns = {"/main"})
public class MainServlet extends HttpServlet {
    private BouquetService bouquetService = new BouquetService();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        request.setAttribute("flowers", bouquetService.getFlowerList());
        request.setAttribute("designs", bouquetService.getDesignList());

        getServletContext().getRequestDispatcher("/main.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Long designId = Long.parseLong(request.getParameter("designId"));
        String[] selectedFlower = request.getParameterValues("flowerId");
        String[] flowerNums = request.getParameterValues("flowerNum");

        HttpSession session = request.getSession();
        session.setMaxInactiveInterval(3600);

        Bouquet bouquet = bouquetService.initBouquet(designId, selectedFlower, flowerNums);
        session.setAttribute("bouquetList", bouquetService.addBouquetInList(bouquet.getUuid(), bouquet));

        response.sendRedirect("/order");
    }
}