package com.example.controller;

import com.example.model.entity.Bouquet;
import com.example.model.entity.Order;
import com.example.model.entity.User;
import com.example.service.BouquetService;
import com.example.service.OrderService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;
import java.util.Map;

@WebServlet(urlPatterns = {"/order"})
public class OrderServlet extends HttpServlet {
    private OrderService orderService = new OrderService();
    private BouquetService bouquetService = new BouquetService();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        session.getAttribute("bouquetList");

        getServletContext().getRequestDispatcher("/order.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("user");

        String uuid = request.getParameter("bouquetId");
        String name = request.getParameter("name");
        String surname = request.getParameter("surname");
        String address = request.getParameter("address");
        String phoneNumber = request.getParameter("phoneNumber");

        Map<String, Bouquet> bouquets = (Map<String, Bouquet>) session.getAttribute("bouquetList");
        if (uuid!=null) {
            bouquets.remove(uuid);
            getServletContext().getRequestDispatcher("/order.jsp").forward(request, response);
        }

        if(name.equals("") | surname.equals("") | address.equals("") | phoneNumber.equals("") | bouquets.isEmpty()) {
            request.setAttribute("emptyMessage", "Fields cannot be empty");
            request.getRequestDispatcher("/order.jsp").forward(request, response);
        } else {
            if (user != null) {
                Order order = orderService.addNewOrder(name, surname, address, phoneNumber, user, bouquets);
                for (Bouquet bouquet : orderService.getBouquetList(bouquets)) {
                    bouquetService.addNewBouquet(bouquet, order);
                }
            } else {
                Order order = orderService.addNewOrder(name, surname, address, phoneNumber, bouquets);
                for (Bouquet bouquet : orderService.getBouquetList(bouquets)) {
                    bouquetService.addNewBouquet(bouquet, order);
                }
            }
        }
        request.getRequestDispatcher("successful.jsp").forward(request, response);
    }
}
