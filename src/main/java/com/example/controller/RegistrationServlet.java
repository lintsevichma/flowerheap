package com.example.controller;

import com.example.service.UserService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns = {"/registration"})
public class RegistrationServlet extends HttpServlet {
    private UserService userService = new UserService();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("/registration.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        String email = request.getParameter("email");

        if (userService.emailValid(email)) {
            request.setAttribute("emailMessage", "Username exists. Try another username");
            request.getRequestDispatcher("/registration.jsp").forward(request, response);
        } else if(password.equals("") | username.equals("") | email.equals("")) {
            request.setAttribute("emptyMessage", "Fields cannot be empty");
            request.getRequestDispatcher("/registration.jsp").forward(request, response);
        } else {
            userService.addNewUser(username, password, email);
            response.sendRedirect("/login");
        }
    }
}