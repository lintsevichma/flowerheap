package com.example.dao;

import com.example.configuration.DataSourceConfiguration;
import com.example.model.entity.Flower;
import com.example.model.enums.Color;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class FlowerDao implements InterfaceDao<Flower, Long> {

    private static final String CREATE_FLOWER = "INSERT INTO flower (name, color, price) VALUES (?, ?, ?);";
    private static final String SELECT_FLOWER = "SELECT id FROM flower;";
    private static final String SELECT_ONE_FLOWER = "SELECT * FROM flower WHERE id=?;";
    private static final String UPDATE_FLOWER = "UPDATE flower SET name=?, color=?, price=? WHERE id=?;";
    private static final String DELETE_FLOWER = "DELETE * FROM flower WHERE id=?;";

    @Override
    public void create(Flower flower) {
        try (
                Connection connection = DataSourceConfiguration.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(CREATE_FLOWER)
        ) {
            preparedStatement.setString(1, flower.getType());
            preparedStatement.setString(2, flower.getColor().getTitle());
            preparedStatement.setFloat(3, flower.getPrice());

            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<Flower> getAll() {
        List<Flower> flowerList = new ArrayList<>();
        try (
                Connection connection = DataSourceConfiguration.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(SELECT_FLOWER);
                ResultSet resultSet = preparedStatement.executeQuery()
        ) {
            while (resultSet.next()) {
                flowerList.add(getByKey(resultSet.getLong("id")));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return flowerList;
    }

    @Override
    public Flower getByKey(Long id) {
        Flower flower = new Flower();
        try (
                Connection connection = DataSourceConfiguration.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ONE_FLOWER)
        ) {
            preparedStatement.setLong(1, id);

            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                flower.setId(resultSet.getLong("id"));
                flower.setType(resultSet.getString("type"));
                flower.setColor(Color.valueOf(resultSet.getString("color")));
                flower.setPrice(resultSet.getFloat("price"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return flower;
    }

    @Override
    public void update(Flower flower) {
        try (
                Connection connection = DataSourceConfiguration.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_FLOWER)
        ) {
            preparedStatement.setString(1, flower.getType());
            preparedStatement.setString(2, flower.getColor().getTitle());
            preparedStatement.setFloat(3, flower.getPrice());
            preparedStatement.setLong(4, flower.getId());

            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void delete(Flower flower) {
        try (
                Connection connection = DataSourceConfiguration.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(DELETE_FLOWER)
        ) {
            preparedStatement.setLong(1, flower.getId());

            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}