package com.example.dao;

import com.example.configuration.DataSourceConfiguration;
import com.example.model.entity.Design;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DesignDao implements InterfaceDao<Design, Long> {

    private static final String CREATE_DESIGN = "INSERT INTO design (type, price) VALUES (?, ?);";
    private static final String SELECT_DESIGN = "SELECT id FROM design;";
    private static final String SELECT_ONE_DESIGN = "SELECT * FROM design WHERE id=?;";
    private static final String UPDATE_DESIGN = "UPDATE design SET type=?, price=? WHERE id=?;";
    private static final String DELETE_DESIGN = "DELETE design WHERE id=?;";

    @Override
    public void create(Design design) {
        try (
                Connection connection = DataSourceConfiguration.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(CREATE_DESIGN)
        ) {
            preparedStatement.setString(1, design.getType());
            preparedStatement.setFloat(2, design.getPrice());

            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<Design> getAll() {
        List<Design> designList = new ArrayList<>();
        try (
                Connection connection = DataSourceConfiguration.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(SELECT_DESIGN);
                ResultSet resultSet = preparedStatement.executeQuery()
        ) {
            while (resultSet.next()) {
                designList.add(getByKey(resultSet.getLong("id")));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return designList;
    }

    @Override
    public Design getByKey(Long id) {
        Design design = new Design();
        try (
                Connection connection = DataSourceConfiguration.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ONE_DESIGN)
        ) {
            preparedStatement.setLong(1, id);

            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                design.setId(resultSet.getLong("id"));
                design.setType(resultSet.getString("type"));
                design.setPrice(resultSet.getFloat("price"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return design;
    }

    @Override
    public void update(Design design) {
        try (Connection connection = DataSourceConfiguration.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_DESIGN)
        ) {
            preparedStatement.setString(1, design.getType());
            preparedStatement.setFloat(2, design.getPrice());
            preparedStatement.setLong(3, design.getId());

            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void delete(Design design) {
        try (
                Connection connection = DataSourceConfiguration.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(DELETE_DESIGN)
        ) {
            preparedStatement.setString(1, design.getType());
            preparedStatement.setFloat(2, design.getPrice());
            preparedStatement.setLong(3, design.getId());

            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}