package com.example.dao;

import com.example.configuration.DataSourceConfiguration;
import com.example.model.FlowerInBouquet;
import com.example.model.entity.Bouquet;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class BouquetDao implements InterfaceDao<Bouquet, Long> {
    private OrderDao orderDao = new OrderDao();
    private FlowerDao flowerDao = new FlowerDao();
    private DesignDao designDao = new DesignDao();

    private static final String CREATE_BOUQUET =
            "INSERT INTO bouquet (design_id, order_id, quantity) VALUES (?, ?, ?) RETURNING id;";

    private static final String INSERT_FLOWER_IN_BOUQUET =
            "INSERT INTO flower_in_bouquet (bouquet_id, flower_id, flower_num) VALUES (?, ?, ?);";

    private static final String SELECT_BOUQUETS =
            "SELECT id FROM bouquet;";

    private static final String SELECT_ONE_BOUQUET =
            "SELECT * FROM bouquet WHERE id=?;";

    private static final String UPDATE_BOUQUET =
            "UPDATE bouquet SET design_id=?, order_id=?, quantity=? WHERE id=?;";

    private static final String DELETE_BOUQUET =
            "DELETE FROM bouquet WHERE id=?;";

    private static final String SELECT_FLOWER_IN_BOUQUET =
            "SELECT F.id, F.type, F.color, F.price, FiB.flower_num\n" +
                    "FROM flower AS F\n" +
                    "JOIN flower_in_bouquet AS FiB\n" +
                    "ON F.id = FiB.flower_id\n" +
                    "WHERE FiB.bouquet_id = ?;";

    @Override
    public void create(Bouquet bouquet) {
        try (
                Connection connection = DataSourceConfiguration.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(CREATE_BOUQUET)
        ) {
            preparedStatement.setLong(1, bouquet.getDesign().getId());
            preparedStatement.setLong(2, bouquet.getOrder().getId());
            preparedStatement.setInt(3, bouquet.getQuantity());
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                bouquet.setId(resultSet.getLong("id"));
            }
            for (FlowerInBouquet flowerInBouquet : bouquet.getFlowerList()) {
                try (
                        PreparedStatement flowerPreparedStatement = connection.prepareStatement(INSERT_FLOWER_IN_BOUQUET)
                ) {
                    flowerPreparedStatement.setLong(1, bouquet.getId());
                    flowerPreparedStatement.setLong(2, flowerInBouquet.getFlower().getId());
                    flowerPreparedStatement.setInt(3, flowerInBouquet.getFlowerNum());

                    flowerPreparedStatement.executeUpdate();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<Bouquet> getAll() {
        List<Bouquet> bouquetList = new ArrayList<>();
        try (
                Connection connection = DataSourceConfiguration.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(SELECT_BOUQUETS);
                ResultSet resultSet = preparedStatement.executeQuery()
        ) {
            while (resultSet.next()) {
                bouquetList.add(getByKey(resultSet.getLong("id")));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return bouquetList;
    }

    @Override
    public Bouquet getByKey(Long id) {
        Bouquet bouquet = new Bouquet();
        try (
                Connection connection = DataSourceConfiguration.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ONE_BOUQUET)
        ) {
            preparedStatement.setLong(1, id);

            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                bouquet.setId(resultSet.getLong("id"));
                bouquet.setDesign(designDao.getByKey(resultSet.getLong("design_id")));
                bouquet.setOrder(orderDao.getByKey(resultSet.getLong("order_id")));
                bouquet.setFlowerList(findAllFlowersInBouquetByBouquetId(bouquet.getId()));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return bouquet;
    }

    @Override
    public void update(Bouquet bouquet) {
        try (
                Connection connection = DataSourceConfiguration.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_BOUQUET)
        ) {
            preparedStatement.setLong(1, bouquet.getDesign().getId());
            preparedStatement.setLong(2, bouquet.getOrder().getId());
            preparedStatement.setInt(3, bouquet.getQuantity());

            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void delete(Bouquet bouquet) {
        try (
                Connection connection = DataSourceConfiguration.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(DELETE_BOUQUET)
        ) {
            preparedStatement.setLong(1, bouquet.getId());

            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private List<FlowerInBouquet> findAllFlowersInBouquetByBouquetId(Long id) {
        List<FlowerInBouquet> flowersInBouquet = new ArrayList<>();
        try (
                Connection connection = DataSourceConfiguration.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(SELECT_FLOWER_IN_BOUQUET)
        ) {
            preparedStatement.setLong(1, id);

            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                FlowerInBouquet flowerInBouquet = new FlowerInBouquet();
                flowerInBouquet.setFlower(flowerDao.getByKey(resultSet.getLong("id")));
                flowerInBouquet.setFlowerNum(resultSet.getInt("flower_num"));

                flowersInBouquet.add(flowerInBouquet);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return flowersInBouquet;
    }
}