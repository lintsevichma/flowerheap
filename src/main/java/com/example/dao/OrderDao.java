package com.example.dao;

import com.example.configuration.DataSourceConfiguration;
import com.example.model.entity.Bouquet;
import com.example.model.entity.Order;
import jdk.internal.org.objectweb.asm.Type;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class OrderDao {
    private UserDao userDao = new UserDao();

    private static final String CREATE_ORDER = "INSERT INTO ordr (name, surname, address, phone_number) VALUES (?, ?, ?, ?) RETURNING id;";
    private static final String CREATE_ORDER_WITH_USER = "INSERT INTO ordr (user_id, name, surname, address, phone_number) VALUES (?, ?, ?, ?, ?) RETURNING id;";
    private static final String SELECT_ORDER = "SELECT id FROM ordr;";
    private static final String SELECT_ONE_ORDER = "SELECT * FROM ordr WHERE id=?;";
    private static final String UPDATE_ORDER = "UPDATE ordr SET user_id=?, name=?, surname=?, address=?, phone_number=? WHERE id=?;";
    private static final String DELETE_ORDER = "DELETE FROM ordr WHERE id=?;";

    public Order create(Order order) {
         try (
                 Connection connection = DataSourceConfiguration.getConnection();
                 PreparedStatement preparedStatement = connection.prepareStatement(CREATE_ORDER)
         ) {
             preparedStatement.setString(1, order.getName());
             preparedStatement.setString(2, order.getSurname());
             preparedStatement.setString(3, order.getAddress());
             preparedStatement.setString(4, order.getPhoneNumber());
             ResultSet resultSet = preparedStatement.executeQuery();
             while (resultSet.next()) {
                 order.setId(resultSet.getLong("id"));
             }
         } catch (SQLException e) {
             e.printStackTrace();
         }
         return order;
    }
    public Order createWithUser(Order order) {
        try (
                Connection connection = DataSourceConfiguration.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(CREATE_ORDER_WITH_USER)
        ) {
            preparedStatement.setLong(1, order.getUser().getId());
            preparedStatement.setString(2, order.getName());
            preparedStatement.setString(3, order.getSurname());
            preparedStatement.setString(4, order.getAddress());
            preparedStatement.setString(5, order.getPhoneNumber());
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                order.setId(resultSet.getLong("id"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return order;
    }

    public List<Order> getAll() {
        List<Order> orderList = new ArrayList<>();
        try (
                Connection connection = DataSourceConfiguration.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ORDER);
                ResultSet resultSet = preparedStatement.executeQuery()
        ) {
            while (resultSet.next()) {
                orderList.add(getByKey(resultSet.getLong("id")));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return orderList;
    }

    public Order getByKey(Long id) {
        Order order = new Order();
        try (
                Connection connection = DataSourceConfiguration.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ONE_ORDER)
        ) {
            preparedStatement.setLong(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                order.setUser(userDao.getByKey(resultSet.getLong("user_id")));
                order.setName(resultSet.getString("name"));
                order.setSurname(resultSet.getString("surname"));
                order.setAddress(resultSet.getString("address"));
                order.setPhoneNumber(resultSet.getString("phone_number"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return order;
    }

    public void update(Order order) {
        try (
                Connection connection = DataSourceConfiguration.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_ORDER)
        ) {
            preparedStatement.setLong(1, order.getUser().getId());
            preparedStatement.setString(2, order.getName());
            preparedStatement.setString(3, order.getSurname());
            preparedStatement.setString(4, order.getAddress());
            preparedStatement.setString(5, order.getPhoneNumber());
            preparedStatement.setLong(6, order.getId());

            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void delete(Order order) {
        try (
                Connection connection = DataSourceConfiguration.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(DELETE_ORDER)
        ) {
            preparedStatement.setLong(1, order.getId());

            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}