package com.example.dao;

import com.example.configuration.DataSourceConfiguration;
import com.example.model.entity.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UserDao implements InterfaceDao<User, Long> {

    private static final String CREATE_USER = "INSERT INTO usr (username, password, active, email, activation_code, discount) VALUES (?, ?, ?, ?, ?, ?);";
    private static final String SELECT_USERS = "SELECT id FROM usr;";
    private static final String SELECT_USER_BY_ID = "SELECT * FROM usr WHERE id=?;";
    private static final String UPDATE_USER = "UPDATE usr SET username=?, password=?, active=?, email=?, activation_code=?, discount=? WHERE id=?;";
    private static final String DELETE_USER = "DELETE * FROM usr WHERE id=?;";

    private static final String FIND_USER_BY_ID = "SELECT id FROM usr WHERE email=?;";
    private static final String LOGIN_VALID = "SELECT password FROM usr WHERE email=? AND password=?;";
    private static final String USER_EXISTS = "SELECT count(*) FROM usr WHERE email=?;";

    @Override
    public void create(User user) {
        try (
                Connection connection = DataSourceConfiguration.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(CREATE_USER)
        ) {
            preparedStatement.setString(1, user.getUsername());
            preparedStatement.setString(2, user.getPassword());
            preparedStatement.setBoolean(3, user.isActive());
            preparedStatement.setString(4, user.getEmail());
            preparedStatement.setString(5, user.getActivationCode());
            preparedStatement.setFloat(6, user.getDiscount());

            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<User> getAll() {
        List<User> userList = new ArrayList<>();
        try (
                Connection connection = DataSourceConfiguration.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(SELECT_USERS);
                ResultSet resultSet = preparedStatement.executeQuery()
        ) {
            while (resultSet.next()) {
                userList.add(getByKey(resultSet.getLong("id")));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return userList;
    }

    @Override
    public User getByKey(Long id) {
        User user = new User();
        try(
                Connection connection = DataSourceConfiguration.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(SELECT_USER_BY_ID)
        ) {
            preparedStatement.setLong(1, id);

            ResultSet resultSet = preparedStatement.executeQuery();
            while(resultSet.next()) {
                user.setId(resultSet.getLong("id"));
                user.setUsername(resultSet.getString("username"));
                user.setPassword(resultSet.getString("password"));
                user.setActive(resultSet.getBoolean("active"));
                user.setEmail(resultSet.getString("email"));
                user.setActivationCode(resultSet.getString("activation_code"));
                user.setDiscount(resultSet.getFloat("discount"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return user;
    }

    @Override
    public void update(User user) {
        try (
                Connection connection = DataSourceConfiguration.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_USER)
        ) {
            preparedStatement.setString(1, user.getUsername());
            preparedStatement.setString(2, user.getPassword());
            preparedStatement.setBoolean(3, user.isActive());
            preparedStatement.setString(4, user.getEmail());
            preparedStatement.setString(5, user.getActivationCode());
            preparedStatement.setFloat(6, user.getDiscount());
            preparedStatement.setLong(7, user.getId());

            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void delete(User user) {
        try (
                Connection connection = DataSourceConfiguration.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(DELETE_USER)
        ) {
            preparedStatement.setLong(1, user.getId());

            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Long findUserByEmail(String email) {
        Long id = null;
        try (
                Connection connection = DataSourceConfiguration.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(FIND_USER_BY_ID)
        ) {
            preparedStatement.setString(1, email);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                id = resultSet.getLong("id");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return id;
    }

    public boolean loginValid(String email, String password) {
        try (
                Connection connection = DataSourceConfiguration.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(LOGIN_VALID)
        ) {
            preparedStatement.setString(1, email);
            preparedStatement.setString(2, password);

            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                if (resultSet.getString(1).equals(password)) {
                    return true;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean userExists(String email) {
        try (
                Connection connection = DataSourceConfiguration.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(USER_EXISTS)
        ) {
            preparedStatement.setString(1, email);

            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                if (resultSet.getInt(1) == 1) {
                    return true;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
}