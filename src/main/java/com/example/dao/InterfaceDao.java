package com.example.dao;

import java.sql.SQLException;
import java.util.List;
import java.util.Set;

public interface InterfaceDao<Entity, Key> {

    void create(Entity entity) throws SQLException;

    List<Entity> getAll() throws SQLException;
    Entity getByKey(Key key) throws SQLException;

    void update(Entity entity) throws SQLException;

    void delete(Entity entity) throws SQLException;
}