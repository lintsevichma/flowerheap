package com.example.configuration;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import java.sql.Connection;
import java.sql.SQLException;

public class DataSourceConfiguration {
    private static HikariConfig hikariConfig = new HikariConfig();
    private static HikariDataSource dataSource;

    static {
        hikariConfig.setDriverClassName( "org.postgresql.Driver" );
        hikariConfig.setJdbcUrl( "jdbc:postgresql://localhost:5432/flowerHeap" );
        hikariConfig.setUsername( "postgres" );
        hikariConfig.setPassword( "123" );
        hikariConfig.addDataSourceProperty( "cachePrepStmts" , "true" );
        hikariConfig.addDataSourceProperty( "prepStmtCacheSize" , "250" );
        hikariConfig.addDataSourceProperty( "prepStmtCacheSqlLimit" , "2048" );
        dataSource = new HikariDataSource(hikariConfig);
    }

    private DataSourceConfiguration() {}

    public static Connection getConnection() throws SQLException {
        return dataSource.getConnection();
    }
}
