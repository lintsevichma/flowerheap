package com.example.model.entity;

import java.util.List;
import java.util.Objects;

public class Order {

    private Long id;
    private String name;
    private String surname;
    private String address;
    private String phoneNumber;
    private User user;

    private List<Bouquet> bouquets;

    public Order() {
    }

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }
    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getAddress() {
        return address;
    }
    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }
    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public User getUser() {
        return user;
    }
    public void setUser(User user) {
        this.user = user;
    }

    public List<Bouquet> getBouquets() {
        return bouquets;
    }
    public void setBouquets(List<Bouquet> bouquets) {
        this.bouquets = bouquets;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Order order = (Order) o;
        return Objects.equals(id, order.id) &&
                Objects.equals(name, order.name) &&
                Objects.equals(surname, order.surname) &&
                Objects.equals(address, order.address) &&
                Objects.equals(phoneNumber, order.phoneNumber) &&
                Objects.equals(user, order.user) &&
                Objects.equals(bouquets, order.bouquets);
    }
    @Override
    public int hashCode() {
        return Objects.hash(id, name, surname, address, phoneNumber, user, bouquets);
    }
    @Override
    public String toString() {
        return "Order{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", address='" + address + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", user=" + user +
                ", bouquets=" + bouquets +
                '}';
    }
}