package com.example.model.entity;

import com.example.model.FlowerInBouquet;

import java.util.List;
import java.util.Objects;

public class Bouquet {

    private Long id;
    private String uuid;
    private Design design;
    private Order order;
    private Integer quantity;

    private List<FlowerInBouquet> flowerList;

    public Bouquet() {
    }

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    public String getUuid() {
        return uuid;
    }
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public Design getDesign() {
        return design;
    }
    public void setDesign(Design design) {
        this.design = design;
    }

    public Order getOrder() {
        return order;
    }
    public void setOrder(Order order) {
        this.order = order;
    }

    public Integer getQuantity() {
        return quantity;
    }
    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public List<FlowerInBouquet> getFlowerList() {
        return flowerList;
    }
    public void setFlowerList(List<FlowerInBouquet> flowerList) {
        this.flowerList = flowerList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Bouquet bouquet = (Bouquet) o;
        return Objects.equals(id, bouquet.id) &&
                Objects.equals(uuid, bouquet.uuid) &&
                Objects.equals(design, bouquet.design) &&
                Objects.equals(order, bouquet.order) &&
                Objects.equals(quantity, bouquet.quantity) &&
                Objects.equals(flowerList, bouquet.flowerList);
    }
    @Override
    public int hashCode() {
        return Objects.hash(id, uuid, design, order, quantity, flowerList);
    }
    @Override
    public String toString() {
        return "Bouquet{" +
                "id=" + id +
                ", uuid='" + uuid + '\'' +
                ", design=" + design +
                ", order=" + order +
                ", quantity=" + quantity +
                ", flowerList=" + flowerList +
                '}';
    }
}