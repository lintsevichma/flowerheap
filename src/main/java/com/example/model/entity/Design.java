package com.example.model.entity;

import java.util.List;
import java.util.Objects;

public class Design {

    private Long id;
    private String type;
    private Float price;
    private List<Bouquet> bouquets;

    public Design() {
    }

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }

    public Float getPrice() {
        return price;
    }
    public void setPrice(Float price) {
        this.price = price;
    }

    public List<Bouquet> getBouquets() {
        return bouquets;
    }
    public void setBouquets(List<Bouquet> bouquets) {
        this.bouquets = bouquets;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Design design = (Design) o;
        return Objects.equals(id, design.id) &&
                Objects.equals(type, design.type) &&
                Objects.equals(price, design.price) &&
                Objects.equals(bouquets, design.bouquets);
    }
    @Override
    public int hashCode() {
        return Objects.hash(id, type, price, bouquets);
    }
    @Override
    public String toString() {
        return "Design{" +
                "id=" + id +
                ", type='" + type + '\'' +
                ", price=" + price +
                ", bouquets=" + bouquets +
                '}';
    }
}