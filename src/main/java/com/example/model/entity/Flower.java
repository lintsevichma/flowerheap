package com.example.model.entity;

import com.example.model.enums.Color;

import java.util.Objects;

public class Flower {

    private Long id;
    private String type;
    private Color color;
    private Float price;

    public Flower() {
    }

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }

    public Color getColor() {
        return color;
    }
    public void setColor(Color color) {
        this.color = color;
    }

    public Float getPrice() {
        return price;
    }
    public void setPrice(Float price) {
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Flower flower = (Flower) o;
        return Objects.equals(id, flower.id) &&
                Objects.equals(type, flower.type) &&
                color == flower.color &&
                Objects.equals(price, flower.price);
    }
    @Override
    public int hashCode() {
        return Objects.hash(id, type, color, price);
    }
    @Override
    public String toString() {
        return "Flower{" +
                "id=" + id +
                ", name='" + type + '\'' +
                ", color=" + color +
                ", price=" + price +
                '}';
    }
}