package com.example.model;

import com.example.model.entity.Bouquet;
import com.example.model.entity.Flower;

import java.util.Objects;

public class FlowerInBouquet {
    private Bouquet bouquet;
    private Flower flower;
    private Integer flowerNum;

    public FlowerInBouquet() {
    }
    public FlowerInBouquet(Flower flower, Integer flowerNum) {
        this.flower = flower;
        this.flowerNum = flowerNum;
    }

    public Flower getFlower() {
        return flower;
    }
    public void setFlower(Flower flower) {
        this.flower = flower;
    }

    public Bouquet getBouquet() {
        return bouquet;
    }
    public void setBouquet(Bouquet bouquet) {
        this.bouquet = bouquet;
    }

    public Integer getFlowerNum() {
        return flowerNum;
    }
    public void setFlowerNum(Integer flowerNum) {
        this.flowerNum = flowerNum;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FlowerInBouquet that = (FlowerInBouquet) o;
        return Objects.equals(bouquet, that.bouquet) &&
                Objects.equals(flower, that.flower) &&
                Objects.equals(flowerNum, that.flowerNum);
    }
    @Override
    public int hashCode() {
        return Objects.hash(bouquet, flower, flowerNum);
    }
    @Override
    public String toString() {
        return "FlowerInBouquet{" +
                "bouquet=" + bouquet +
                ", flower=" + flower +
                ", quantity=" + flowerNum +
                '}';
    }
}