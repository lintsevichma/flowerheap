package com.example.model.enums;

public enum Color {
    RED ("red"),
    ORANGE ("orange"),
    YELLOW ("yellow"),
    GREEN ("green"),
    BLUE ("blue"),
    PURPLE ("purple"),
    WHITE ("white"),
    PINK ("pink"),
    GREY ("grey"),
    BLACK ("black");

    private String title;

    Color(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    @Override
    public String toString() {
        return "Color{" +
                "title='" + title + '\'' +
                '}';
    }
}